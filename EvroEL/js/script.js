$(document).ready(function() {
    $("#button-top").click(function() {
        $("body,html").animate({
            "scrollTop": "0px"
        }, 700);
    });
    $(window).scroll(function() {
        if ($(window).scrollTop() > $(window).height() / 2) {
            $('#button-top').stop().fadeIn(600);
        } else {
            $('#button-top').stop().fadeOut(600);
        }
    });
    $("#owl-example").owlCarousel({
        items: 4,
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [767, 1],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
        slideSpeed: 200,
        navigation: true,
        navigationText: ["<span class='icon-left-1'></span>", "<span class='icon-right-1'></span>"],
        pagination: false
    });
});